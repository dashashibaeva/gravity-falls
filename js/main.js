function index_page() {
    const root = document.getElementById('root');

    let preview = document.createElement('div');
    root.prepend(preview);
    preview.className = "preview index";

    let previewText = document.createElement('div');
    preview.prepend(previewText);
    previewText.className = "preview_text";
    previewText.innerHTML = `
        <h1 class="h1Plus">
            Найди любимого <br> персонажа <br>  “Gravity Falls”
        </h1>
        <p class="p24">
            Вы сможете узнать тип героев, их способности,
            <br> сильные стороны и недостатки.
        </p>
        <a href="characters.html" class="btn btn_orange btn_home">Начать</a>
    `;
}
index_page();
