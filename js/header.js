function header() {
    let header = document.createElement('header');
    document.body.prepend(header);
    header.className = "header";

    header.innerHTML = `
            <ul class="menu">
                <li class="menu__item logo"></li>
                <li class="menu__item nav">
                    <a href="index.html">Главная</a>
                </li>
                
                <li class="menu__item nav">
                    <a href="characters.html">Персонажи</a>
                </li>
            </ul>
        `;
}
header();